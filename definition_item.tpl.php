<?php
// $Id$

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $definition: the (sanitized) definition.
 * - $explanation: the (sanitized) explanation of the definition.
 * - $did: the unique identifier (ID) of the definition.
 * - $classes: String of classes that can be used to style contextually through
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * @see template_preprocess_definition_item()
 */
?>
<dfn title="<?php print $explanation; ?>" class="<?php print $classes; ?>"><?php print $definition; ?></dfn>
