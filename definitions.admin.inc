<?php
// $Id$

/**
 * @file
 *
 * Administration file for the definitions module.
 */

/**
 * Page callback for the administration page.
 */
function definitions_page() {
  $definitions = definitions_load_mutiple();

  // Olny proceed if we're having content to show.
  if (empty($definitions)) {
    return t('There are no definitions yet. Click the \'Add definition\' link to add one.');
  }

  // Display a notification mesage if there are no input formats configured with this filter yet.
  if (!db_query_range("SELECT name FROM {filter} WHERE name = :name AND status = 1", 0, 1, array(':name' => 'definitions'))->fetchField()) {
    drupal_set_message(t('There are no text formats configured to use the definitions filter yet! To actually use the definitions defined here, you need to enable the definitions filter in one of the site\'s used !link.', array('!link' => l(t('text formats'), 'admin/config/content/formats'))), 'warning');
  }

  $rows = array();
  $first_character = '';
  // Iterate over each found definition and create an array element for each first character.
  foreach ($definitions as $did => $definition) {
    $curr_first_character = drupal_strtolower(drupal_substr($definition->definition, 0, 1));

    if ($curr_first_character !== $first_character) {
      $first_character = $curr_first_character;
      $rows[$first_character] = array();
    }
    $rows[$first_character][$did] = $definition;
  }
  // Return a rendarable array.
  return array('#markup' => theme('definitions_page', $rows));
}

/**
 * Theme the definitions administration page.
 */
function theme_definitions_page($rows) {

  $output = '<p><a name="top"></a>';
  $occuring_characters = array_keys($rows);

  foreach ($occuring_characters as $i => $character) {
    $capital = drupal_strtoupper($character);
    $output .= (in_array($character, $occuring_characters) ? l($capital, $_GET['q'], array('fragment' => $character)) : $capital);
    $output .= $i < count($occuring_characters) - 1 ? ' | ' : '';
  }

  foreach ($rows as $character => $definitions) {
    $output .= '<h2 name="' . $character . '" id="' . $character . '">' . drupal_strtoupper($character) . '</h2>';
    $output .= '<dl>';

    foreach ($definitions as $did => $definition) {
      $output .= '<dt id="' . $definition->definition . '">' . l($definition->definition, 'admin/config/content/definitions/' . $did . '/edit') . '</dt>';
      $output .= '<dd>' . $definition->explanation . '</dd>';
    }

    $output .= '</dl>';
    $output .= '<p><sub>' . l(t('Back to Top'), $_GET['q'], array('fragment' => 'top')) . '</sub></p>';
  }
  return $output;
}

/**
 * Add- or edit form for a single definition.
 */
function definitions_form($form, $form_state, $definition = NULL) {
  $form = array('definition' => array('#tree' => TRUE));

  $form['definition']['did'] = array(
    '#type' => 'value',
    '#value' => ($definition ? $definition->did : NULL),
  );
  $form['definition']['definition'] = array(
    '#type' => 'textfield',
    '#title' => t('Definition'),
    '#default_value' => ($definition ? $definition->definition : ''),
    '#required' => TRUE,
  );
  $form['definition']['explanation'] = array(
    '#type' => 'textarea',
    '#title' => t('Explanation'),
    '#default_value' => ($definition ? $definition->explanation : ''),
    '#required' => TRUE,
  );
  $form['definition']['extra_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra class'),
    '#default_value' => ($definition ? $definition->extra_class : ''),
    '#description' => t('Optionally it is possible to give the generated markup element an extra CSS class.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));

  return $form;
}

/**
 * Validation callback for the definitions form.
 */
function definitions_form_validate($form, &$form_state) {
  $definition = (object) $form_state['values']['definition'];

  if ($form_state['values']['op'] == t('Delete')) {
    drupal_goto('admin/config/content/definitions/' . $definition->did . '/delete', drupal_get_destination())  ;
  }
  elseif (definition_exists($definition)) {
    form_set_error('definition][definition', t('This definition already exists!'));
  }
}

/**
 * Submit callback for the definitions form.
 */
function definitions_form_submit($form, &$form_state) {
  $definition = (object) $form_state['values']['definition'];
  definition_save($definition);
  $form_state['redirect'] = 'admin/config/content/definitions';
}

/**
 * Form defenition for deleting a definition.
 */
function definitions_delete_form($form, &$form_state, $definition) {
  $form['did'] = array(
    '#type' => 'value',
    '#value' => $definition->did,
  );
  $form['definition'] = array(
    '#type' => 'value',
    '#value' => $definition->definition,
  );
  return confirm_form($form, t('Are you sure you want to delete the definition %definition?', array('%definition' => $definition->definition)), 'admin/config/content/definitions', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Submit callback for definitions_delete_form().
 */
function definitions_delete_form_submit($form, &$form_state) {
  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query("DELETE FROM {definitions} WHERE did = %d", $form_state['values']['did']) */
  db_delete('definitions')
  ->condition('did', $form_state['values']['did'])
  ->execute();
  $form_state['redirect'] = 'admin/config/content/definitions';
  drupal_set_message(t('The definition %definition has been deleted', array('%definition' => $form_state['values']['definition'])));
}
